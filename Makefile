CI_REGISTRY ?= registry.gitlab.com
CI_PROJECT_PATH ?= role_unit/role_unit_containers
TARGETS = $(wildcard archlinux* debian* centos* build*)

containers: $(TARGETS)

$(TARGETS): login
	echo $@
	docker build -t ${CI_REGISTRY}/${CI_PROJECT_PATH}:$@ -f $@/dockerfile $@
	test -f $@/dockerfile_cached && docker build -t ${CI_REGISTRY}/${CI_PROJECT_PATH}:$@_cached -f $@/dockerfile_cached $@ || true
	test "${CI_COMMIT_REF_NAME}" == "master" && docker push ${REGISTRY_PATH} ${CI_REGISTRY}/${CI_PROJECT_PATH}:$@ || true
	test -f $@/dockerfile_cached -a "${CI_COMMIT_REF_NAME}" == "master" && docker push ${REGISTRY_PATH} ${CI_REGISTRY}/${CI_PROJECT_PATH}:$@_cached || true

login:
	test -n "${CI_BUILD_TOKEN}" && docker login -u gitlab-ci-token -p ${CI_BUILD_TOKEN} ${CI_REGISTRY} || true

.PHONY: $(TARGETS)
